# iter

Iterator library for go!


# How

Lots of reflection, but you don't need to worry about that! the main interface `iter` provides is the `iterable` interface

```go
// Iterable represents an iterable object, not safe for concurrent use.
type Iterable interface {
	// Next returns the next item in the iterable, if there are no more items
	// then next returns nil
	Next() reflect.Value
}
```

If a type is iterable, then you can call `Next()` on it! this package provides a handy wrapper `Iter` that will convert a slice, array map or channel to an `Iterable`

Now, if you had to manually call `Next()` and type assert for this to be of any value that would suck! that's why this package provides handy functions that operate on `Iterable`s

```go
func ParRange(i Iterable, par int, fn interface{}) *sync.WaitGroup 
```

ParRange lets you iterate over an iterable concurrently, whoa! its as easy as

```go
// Iter converts the array to an iterable
i := iter.Iter([]int{1, 2, 3, 4, 5, 6, 7, 8, 9})

// iterate over the iterable in 4 goroutines concurrently
iter.ParRange(i, 4, func(n int) {
  fmt.Printf("Got: %d\n", n)
}).Wait()
```

Output (non deterministic)
```
Got: 1
Got: 5
Got: 3
Got: 7
Got: 2
Got: 6
Got: 4
Got: 8
Got: 9
```
