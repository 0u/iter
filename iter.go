// Package iter provides an interface to a generic iterable, and methods to operate on it.
package iter

import (
	"reflect"
	"sync"
)

// TODO: Find a better way, reflect might have a constant for nil.
var null = reflect.ValueOf(nil)

// Iterable represents an Iterable object, not safe for concurrent use.
type Iterable interface {
	// Next returns the next item in the Iterable, if there are no more items
	// then next returns nil
	Next() reflect.Value
}

// Map applies a function to every item in an iterable, and returns a lazy iterable that can be consumed.
// fn MUST return a value for every item processed, otherwise this will panic.
func Map(i Iterable, fn interface{}) Iterable {
	v := reflect.ValueOf(fn)
	if v.Kind() != reflect.Func {
		panic(v.String() + " is not of kind reflect.Func")
	}

	return Func(func() reflect.Value {
		item := i.Next()
		if !item.IsValid() {
			return null
		}
		return v.Call([]reflect.Value{item})[0]
	})
}

// ParRange ranges over an Iterable concurrently.
// i is the Iterable to range over
// par is the amount of goroutines to range over concurrently
// fn is the function to apply to every item.
// returning a waitgroup for the workers.
func ParRange(i Iterable, par int, fn interface{}) *sync.WaitGroup {
	v := reflect.ValueOf(fn)
	if v.Kind() != reflect.Func {
		panic(v.String() + " is not of kind reflect.Func")
	}

	items := make(chan reflect.Value)
	go func() {
		for {
			item := i.Next()
			if !item.IsValid() {
				close(items)
				break
			}
			items <- item
		}
	}()

	wg := new(sync.WaitGroup)
	wg.Add(par)
	for i := 0; i < par; i++ {
		go func() {
			defer wg.Done()
			for item := range items {
				v.Call([]reflect.Value{item})
			}
		}()
	}
	return wg
}

// Chan transforms an Iterable into a channel, in effect this makes it work
// with the built in x := range ... syntax.
func Chan(i Iterable) <-chan reflect.Value {
	c := make(chan reflect.Value)
	go func() {
		for {
			item := i.Next()
			if !item.IsValid() {
				close(c)
				break
			}
			c <- item
		}
	}()
	return c
}

// Func wraps a function as an Iterable
type Func func() reflect.Value

// Next simply calls the wrapped function to get the next element.
func (i Func) Next() reflect.Value { return i() }

// Iter creates an Iterable from a base type, such as []int chan int or map[T]int
func Iter(a interface{}) (i Iterable) {
	v := reflect.ValueOf(a)
	t := v.Type()

	switch t.Kind() {
	case reflect.Chan:
		i = Func(func() reflect.Value {
			for {
				x, ok := v.Recv()
				if !ok {
					return null
				}
				return x
			}
		})
	case reflect.Array, reflect.Slice:
		idx := 0
		i = Func(func() reflect.Value {
			if idx < v.Len() {
				item := v.Index(idx)
				idx++
				return item
			}
			return null
		})
	case reflect.Map:
		m := v.MapRange()
		i = Func(func() reflect.Value {
			if m.Next() {
				return m.Value()
			}
			return null
		})
	default:
		panic(t.String() + " is not Iterable")
	}

	return i
}
