package iter

import (
	"reflect"
	"testing"
)

func assertEQ(t *testing.T, a, b int) {
	if a != b {
		t.Fatalf("want %d, got %d", b, a)
	}
}

type intArray struct {
	array []int
	index int
}

func (i *intArray) Next() reflect.Value {
	if i.index == len(i.array) {
		return reflect.ValueOf(nil)
	}

	v := i.array[i.index]
	i.index++
	return reflect.ValueOf(v)
}

func TestChan(t *testing.T) {
	tests := []struct {
		name string
		iter Iterable
		c    <-chan reflect.Value
	}{
		{
			"Iter primitive",
			Iter([]int{1, 2, 3, 4, 5}),
			Chan(Iter([]int{1, 2, 3, 4, 5})),
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {
			for {
				v1 := tc.iter.Next()
				v2, ok := <-tc.c
				if v1.IsValid() != ok {
					t.Fatalf("iter returned %v chan returned %v", v1, v2)
				}
				if !v1.IsValid() && !ok {
					break
				}

				assertEQ(t, v1.Interface().(int), v2.Interface().(int))
			}
		})
	}
}

func TestMap(t *testing.T) {
	tests := []struct {
		name string
		iter Iterable
	}{
		{
			// First test with type iter (primitive iterators)
			"Int array",
			Iter([]int{1, 2, 3}),
		},
		{
			// Second test, with custom iterator
			"Custom iterable",
			&intArray{[]int{1, 2, 3}, 0},
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {
			got := Map(tc.iter, func(n int) int {
				return n + 10
			})

			for x := range Chan(got) {
				n := x.Interface().(int)
				t.Logf("got: %d", n)
				if n < 11 || n > 14 {
					t.Fatalf("want range 11-14, got %d", n)
				}
			}
		})
	}
}

func TestParRange(t *testing.T) {
	tests := []struct {
		name string
		iter Iterable
	}{
		{
			// First test with type iter (primitive iterators)
			"Int array",
			Iter([]int{1, 2, 3}),
		},
		{
			// Second test, with custom iterator
			"Custom iterable",
			&intArray{[]int{1, 2, 3}, 0},
		},
	}

	for _, tc := range tests {
		t.Run(tc.name, func(t *testing.T) {
			got := make(chan int)
			wg := ParRange(tc.iter, 4, func(n int) {
				got <- n + 10
			})
			go func() { wg.Wait(); close(got) }()
			for x := range got {
				t.Logf("got: %d", x)
				if x < 11 || x > 14 {
					t.Fatalf("want range 11-14, got %d", x)
				}
			}
		})
	}
}

func TestIterMap(t *testing.T) {
	// Keys don't matter
	a := Iter(map[int]int{
		0:    1,
		109:  2,
		-139: 3,
	})
	assertEQ(t, a.Next().Interface().(int), 1)
	assertEQ(t, a.Next().Interface().(int), 2)
	assertEQ(t, a.Next().Interface().(int), 3)
	if a.Next().IsValid() {
		t.Fatal("a.Next() returned a valid reflect.Value, expected invalid")
	}
}

func TestIterSlice(t *testing.T) {
	a := Iter([]int{1, 2, 3})
	assertEQ(t, a.Next().Interface().(int), 1)
	assertEQ(t, a.Next().Interface().(int), 2)
	assertEQ(t, a.Next().Interface().(int), 3)
	if a.Next().IsValid() {
		t.Fatal("a.Next() returned a valid reflect.Value, expected invalid")
	}
}

func TestIterChan(t *testing.T) {
	c := make(chan int)
	go func() {
		for i := 1; i < 4; i++ {
			c <- i
		}
		close(c)
	}()

	a := Iter(c)
	assertEQ(t, a.Next().Interface().(int), 1)
	assertEQ(t, a.Next().Interface().(int), 2)
	assertEQ(t, a.Next().Interface().(int), 3)
	if a.Next().IsValid() {
		t.Fatal("a.Next() returned a valid reflect.Value, expected invalid")
	}
}
